# Python Typing

This is the first repo for [Codurance's](https://www.codurance.com/) Python circuit 2022 first talk: Static Typing in
Python.

## Useful resources
- [Python typing library](https://docs.python.org/3/library/typing.html)
- [Real Python guide for type checking](https://realpython.com/python-type-checking/)

## Collaborators
- [Javier Martínez Alcantara](https://gitlab.com/Pablompg)
- [Pablo Martínez Pérez](https://gitlab.com/jmaralc)

## PPT
- [PPT Presentation - Work In Progress](https://docs.google.com/presentation/d/1bMhhISC4REvnr_AF9e41WjuBywUAQ_Jyl8hSmLU-wgs/edit#slide=id.gbf3c3ccaa8_0_2153)
